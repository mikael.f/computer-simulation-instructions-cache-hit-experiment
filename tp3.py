# -*- coding: utf-8 -*-

import time
import random
import sys
import os

class BlocoMemoria:
    palavras = []
    endBloco = -1
    custo = 0
    cacheHit = 0
    lastHit = 0
    frequenciaHits = 0
    entradaCache = 0
    atualizado = False

    def getCacheHit(self):
        return self.cacheHit

    def setCacheHit(self, valor):
        self.cacheHit = valor;

    def getCusto(self):
        return self.custo

    def setCusto(self, custo):
        self.custo = custo

    def getPalavras(self):
        return self.palavras

    def setPalavras(self, palavras):
        self.palavras = palavras

    def getEndBloco(self):
        return self.endBloco

    def setEndBloco(self, endBloco):
        self.endBloco = endBloco

    def isAtualizado(self):
        return self.atualizado

    def setAtualizado(self):
        self.atualizado = True

    def setDesatualizado(self):
        self.atualizado = False

    def getLastHit(self):
        return self.lastHit

    def setLastHit(self, lastHit):
        self.lastHit = lastHit

    def getFrequenciaHits(self):
        return self.frequenciaHits

    def incrementaFrequenciaHits(self):
        self.frequenciaHits += 1

    def zeraFrequenciaHits(self):
        self.frequenciaHits = 0

    def getEntradaCache(self):
        return self.entradaCache

    def setEntradaCache(self, entradaCache):
        self.entradaCache = entradaCache

    def zeraEntradaCache(self):
        self.entradaCache = 0

class Endereco:
    endBloco = 0
    endPalavra = 0

    def getEndBloco(self):
        return self.endBloco

    def setEndBloco(self, endBloco):
        self.endBloco = endBloco

    def getEndPalavra(self):
        return self.endPalavra

    def setEndPalavra(self, endPalavra):
        self.endPalavra = endPalavra

class Instrucao:
    end1 = Endereco()
    end2 = Endereco()
    end3 = Endereco()
    opcode = -1

    def getEnd1(self):
        return self.end1

    def setEnd1(self, end1):
        self.end1 = end1

    def getEnd2(self):
        return self.end2

    def setEnd2(self, end2):
        self.end2 = end2

    def getEnd3(self):
        return self.end3

    def setEnd3(self, end3):
        self.end3 = end3

    def getOpcode(self):
        return self.opcode

    def setOpcode(self, opcode):
        self.opcode = opcode

class MMU:
    def __init__(self, algoritmoDeSubstituicaoCache):
        self.algoritmoSubstituicao = algoritmoDeSubstituicaoCache

    def encontrarPosCache(self, cache):
        if self.algoritmoSubstituicao == "LRU":
            return self.LRU(cache)
        elif self.algoritmoSubstituicao == "FIFO":
            return self.FIFO(cache)
        else:
            return self.LFU(cache)

    def buscarNasMemorias(self, endereco, RAM, cache1, cache2, cache3):
        custo = 0

        for bloco in range(len(cache1)):
            if cache1[bloco].getEndBloco() == endereco.getEndBloco():
                custo = 1
                cache1[bloco].setCusto(custo)
                cache1[bloco].setCacheHit(1)
                cache1[bloco].setLastHit(time.perf_counter())
                cache1[bloco].incrementaFrequenciaHits()
                return cache1[bloco]

        for bloco in range(len(cache2)):
            if cache2[bloco].getEndBloco() == endereco.getEndBloco():
                custo = 11
                cache2[bloco].setCusto(custo)
                cache2[bloco].setCacheHit(2)
                cache2[bloco].setLastHit(time.perf_counter())
                cache2[bloco].incrementaFrequenciaHits()

                posCache1 = self.encontrarPosCache(cache1)

                return MMU.moverEntreMemorias(cache2, cache1, bloco, posCache1)

        for bloco in range(len(cache3)):
            if cache3[bloco].getEndBloco() == endereco.getEndBloco():
                custo = 31
                cache3[bloco].setCusto(custo)
                cache3[bloco].setCacheHit(3)
                cache3[bloco].setLastHit(time.perf_counter())
                cache3[bloco].incrementaFrequenciaHits()
                #Movendo da cache3 para a cache2, e da cache2 para a cache1
                posCache2 = self.encontrarPosCache(cache2)
                MMU.moverEntreMemorias(cache3, cache2, bloco, posCache2)
                posCache1 = self.encontrarPosCache(cache1)

                return MMU.moverEntreMemorias(cache2, cache1, posCache2, posCache1)

        #Miss nas 3 caches, buscar da RAM
        for bloco in range(len(RAM)):
            if RAM[bloco].getEndBloco() == endereco.getEndBloco():
                custo = 131

                RAM[bloco].setCusto(custo)
                RAM[bloco].setCacheHit(4)
                RAM[bloco].setLastHit(time.perf_counter())
                RAM[bloco].setEntradaCache(time.perf_counter())
                RAM[bloco].incrementaFrequenciaHits()

                posCache3 = self.encontrarPosCache(cache3)
                MMU.moverEntreMemorias(RAM, cache3, bloco, posCache3)
                posCache2 = self.encontrarPosCache(cache2)
                MMU.moverEntreMemorias(cache3, cache2, posCache3, posCache2)
                posCache1 = self.encontrarPosCache(cache1)
                return MMU.moverEntreMemorias(cache2, cache1, posCache2, posCache1)

        #Miss na RAM, buscar no HD
        custo = 3131
        posRAM = self.encontrarPosCache(RAM)
        #Sobrescrevendo no HD pela RAM

        if(RAM[posRAM].isAtualizado() == True):
            HD = open("HD.bin", "wb")
            HD.seek(RAM[posRAM].getEndBloco())
            HD.write(RAM[posRAM].getPalavras()[0])
            HD.write(RAM[posRAM].getPalavras()[1])
            HD.write(RAM[posRAM].getPalavras()[2])
            HD.write(RAM[posRAM].getPalavras()[3])
            HD.close()
            RAM[posRAM].setDesatualizado()

        #Buscando o conteúdo do HD
        HD = open("HD.bin", "rb")
        HD.seek(endereco.getEndBloco())

        palavras = []
        for x in range(4):
            palavras.append(HD.read(1))
        RAM[posRAM].setEndBloco(endereco.getEndBloco())
        RAM[posRAM].setPalavras(palavras)
        HD.close()

        RAM[posRAM].setCusto(custo)
        RAM[posRAM].setCacheHit(5)
        RAM[posRAM].setLastHit(time.perf_counter())
        RAM[posRAM].setEntradaCache(time.perf_counter())
        RAM[posRAM].zeraFrequenciaHits()
        RAM[posRAM].incrementaFrequenciaHits()
        RAM[posRAM].setDesatualizado()

        posCache3 = self.encontrarPosCache(cache3)
        MMU.moverEntreMemorias(RAM, cache3, posRAM, posCache3)
        posCache2 = self.encontrarPosCache(cache2)
        MMU.moverEntreMemorias(cache3, cache2, posCache3, posCache2)
        posCache1 = self.encontrarPosCache(cache1)
        return MMU.moverEntreMemorias(cache2, cache1, posCache2, posCache1)

    def moverEntreMemorias(memoriaAtual, memoriaDestino, endAtual, endDestino):
        aux = memoriaDestino[endDestino]
        memoriaDestino[endDestino] = memoriaAtual[endAtual]
        memoriaAtual[endAtual] = aux

        return memoriaDestino[endDestino]

    def LRU(self, cache):#Usado a mais tempo
        tempo = time.perf_counter()
        maiorTempo = 0
        posicao = 0

        for i in range(len(cache)):
            if(tempo - cache[i].getLastHit() > maiorTempo):
                maiorTempo = tempo - cache[i].getLastHit()
                posicao = i


        return posicao

    def FIFO(self, cache):#Primeiro a entrar, primeiro a sair
        entradaMaisAntiga = time.perf_counter()
        maisAntigo = 0
        for i in range(len(cache)):
            if cache[i].getEntradaCache() < entradaMaisAntiga:
                maisAntigo = i
                entradaMaisAntiga = cache[i].getEntradaCache()

        return maisAntigo

    def LFU(self, cache):#Usado com menos frequência
        menosUsado = 0
        menorFrequencia = sys.maxsize
        for i in range(len(cache)):
            if cache[i].getFrequenciaHits() < menorFrequencia:
                menosUsado = i
                menorFrequencia = cache[i].getFrequenciaHits()

        return menosUsado



class Maquina:
    def __init__(self,  tamanhoRAM, tamanhoPrograma, qdePalavrasBloco, tamanhoCache1, tamanhoCache2, tamanhoCache3, tamanhoHD):
        self.tamanhoHD = tamanhoHD
        self.tamanhoRAM = tamanhoRAM
        self.tamanhoCache1 = tamanhoCache1
        self.tamanhoCache2 = tamanhoCache2
        self.tamanhoCache3 = tamanhoCache3
        self.tamanhoPrograma = tamanhoPrograma
        self.qdePalavrasBloco = qdePalavrasBloco
        self.iniciarMaquina()


    def setAlgoritmoSubstituicao(self, algoritmoSubstituicao):
        self.algoritmoSubstituicao = algoritmoSubstituicao

    def iniciarMaquina(self):
        self.PC = 0
        self.opcode = 10
        self.custo = 0
        self.missC1 = 0
        self.hitC1 = 0
        self.missC2 = 0
        self.hitC2 = 0
        self.missC3 = 0
        self.hitC3 = 0
        self.hitRAM = 0
        self.missRAM = 0
        self.hitHD = 0
        self.algoritmoSubstituicao = "NULO"
        self.tamanhoPrograma = 0

    def tratarCacheHit(self, cacheHit):
        if cacheHit == 1:
            self.hitC1 += 1
        elif cacheHit == 2:
            self.missC1 += 1
            self.hitC2 += 1
        elif cacheHit == 3:
            self.missC1 += 1
            self.missC2 += 1
            self.hitC3 += 1
        elif cacheHit == 4:
            self.missC1 += 1
            self.missC2 += 1
            self.missC3 += 1
            self.hitRAM += 1
        else:
            self.missC1 += 1
            self.missC2 += 1
            self.missC3 += 1
            self.missRAM += 1
            self.hitHD += 1

    def atualizaCusto(self, custo):
        self.custo += custo

    def imprimirRelatorio(self):
        print("\n")
        print("Cache1: hits = %d / misses = %d" % (self.hitC1, self.missC1))
        print("Cache2: hits = %d / misses = %d" % (self.hitC2, self.missC2))
        print("Cache3: hits = %d / misses = %d" % (self.hitC3, self.missC3))
        print("Custo total do programa = %d" % (self.custo))
        print("\n")

    def montarRAM(self):
        RAM = []
        for i in range(self.tamanhoRAM):
            aux = BlocoMemoria()
            aux.setEndBloco(-1)
            RAM.append(aux)

        return RAM

    def montarCache(self, tamanho):
        cache = []
        for i in range(tamanho):
            aux = BlocoMemoria()
            aux.setEndBloco(-1)
            cache.append(aux)
        return cache

    def montarHD(self):
        with open("HD.bin", "wb") as HD:
            for i in range(self.tamanhoHD):
                for y in range(4):
                    HD.write(os.urandom(1))

    def montarProgramaAleatorio(self, tamanho):
        memoriaInstrucoes = []

        for i in range(tamanho-1):
            inst = Instrucao()
            inst.setOpcode(random.randrange(2))

            end1 = Endereco()
            end1.setEndBloco(random.randrange(self.tamanhoHD))
            end1.setEndPalavra(random.randrange(self.qdePalavrasBloco))
            inst.setEnd1(end1)

            end2 = Endereco()
            end2.setEndBloco(random.randrange(self.tamanhoHD))
            end2.setEndPalavra(random.randrange(self.qdePalavrasBloco))
            inst.setEnd2(end2)

            end3 = Endereco()
            end3.setEndBloco(random.randrange(self.tamanhoHD))
            end3.setEndPalavra(random.randrange(self.qdePalavrasBloco))
            inst.setEnd3(end3)

            memoriaInstrucoes.append(inst)

        inst = Instrucao()
        inst.setOpcode(-1)

        memoriaInstrucoes.append(inst)

        return memoriaInstrucoes

    def gerarProgramaPorArquivo(self):
        memoriaInstrucoes = []

        file = open("C:\python\instructions.txt", "r")

        tamanhoPrograma = int(file.readline())
        self.tamanhoPrograma = tamanhoPrograma
        for linha in file:
            dado = ""
            array = []
            for char in linha:
                if char == " " or char == "\n":
                    array.append(int(dado))
                    dado = ""
                else:
                    dado += char

            inst = Instrucao()
            inst.setOpcode(array[0])

            end1 = Endereco()
            end1.setEndBloco(array[1])
            end1.setEndPalavra(array[2])
            inst.setEnd1(end1)

            end2 = Endereco()
            end2.setEndBloco(array[3])
            end2.setEndPalavra(array[4])
            inst.setEnd2(end2)

            end3 = Endereco()
            end3.setEndBloco(array[5])
            end3.setEndPalavra(array[6])
            inst.setEnd3(end3)

            memoriaInstrucoes.append(inst)

        file.close()

        inst = Instrucao()
        inst.setOpcode(-1)
        memoriaInstrucoes.append(inst)

        return memoriaInstrucoes

    def gerarInterrupcao(self, ctrl, limite):
        programas = []

        if(random.randrange(4) == 0 #25% chance de interrupcao
        and ctrl < limite):
            programas = self.gerarInterrupcao(ctrl + 1, limite)
            tamanho = random.randrange(10,50)
            programas.append(self.montarProgramaAleatorio(tamanho))

        return programas

    def executarInstrucao(self, instrucao, RAM, cache1, cache2, cache3, MMU, tipo):
        opcode = instrucao.getOpcode()

        blocoMemoria1 = MMU.buscarNasMemorias(instrucao.getEnd1(), RAM, cache1, cache2, cache3)
        blocoMemoria2 = MMU.buscarNasMemorias(instrucao.getEnd2(), RAM, cache1, cache2, cache3)
        blocoMemoria3 = MMU.buscarNasMemorias(instrucao.getEnd3(), RAM, cache1, cache2, cache3)

        self.atualizaCusto(blocoMemoria1.getCusto())
        self.atualizaCusto(blocoMemoria2.getCusto())
        self.atualizaCusto(blocoMemoria3.getCusto())

        self.tratarCacheHit(blocoMemoria1.getCacheHit())
        self.tratarCacheHit(blocoMemoria2.getCacheHit())
        self.tratarCacheHit(blocoMemoria3.getCacheHit())

        conteudo1 = blocoMemoria1.getPalavras()[instrucao.getEnd1().getEndPalavra()]
        conteudo2 = blocoMemoria1.getPalavras()[instrucao.getEnd2().getEndPalavra()]


        if opcode == 1:
            if tipo != "tdi":
                interrupcao = self.gerarInterrupcao(0, 2)#interrupcao interna
                for programa in interrupcao:
                    self.lerPrograma(programa, RAM, cache1, cache2, cache3, MMU, "tdi")
            #blocoMemoria3.getPalavras()[instrucao.getEnd3().getEndPalavra()] = (int.from_bytes(conteudo1, 'big')   + int.from_bytes(conteudo2, 'big')).to_bytes(2, 'big')
            blocoMemoria3.setAtualizado()
        else:
            if tipo != "tdi":
                interrupcao = self.gerarInterrupcao(0, 2)#interrupcao interna
                for programa in interrupcao:
                    self.lerPrograma(programa, RAM, cache1, cache2, cache3, MMU, "tdi")
            #blocoMemoria3.getPalavras()[instrucao.getEnd3().getEndPalavra()] = (int.from_bytes(conteudo1, 'big') - int.from_bytes(conteudo2, 'big')).to_bytes(2, 'big', signed = True)
            blocoMemoria3.setAtualizado()

        if tipo != "tdi":
            interrupcao = self.gerarInterrupcao(0, 5)#interrupcao externa
            for programa in interrupcao:
                self.lerPrograma(programa, RAM, cache1, cache2, cache3, MMU, "tdi")

    def lerPrograma(self, programa, RAM, cache1, cache2, cache3, MMU, tipo):
        for instrucao in programa:
            opcode = instrucao.getOpcode()
            if opcode == -1:
                break

            self.executarInstrucao(instrucao, RAM, cache1, cache2, cache3, MMU, tipo)

    def processador(self, tipo):
        gerenciadorMemoria = MMU(self.algoritmoSubstituicao)
        RAM = self.montarRAM()
        self.montarHD()
        cache1 = self.montarCache(self.tamanhoCache1)
        cache2 = self.montarCache(self.tamanhoCache2)
        cache3 = self.montarCache(self.tamanhoCache3)
        memoriaInstrucoes = self.gerarProgramaPorArquivo()

        self.lerPrograma(memoriaInstrucoes, RAM, cache1, cache2, cache3, gerenciadorMemoria, "programa")

        self.imprimirRelatorio()
