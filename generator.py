from random import *

class Generator:
	num_address = [3, 3] # number of addresses for each opcode evaluated from 0 to n-1

	# -------------------------- Code --------------------------


	def __init__(self, size_memory, instructions, size_block, probability, num_words, n):
		self.size_memory = size_memory # HD size
		self.instructions = instructions # number of instructions generated
		self.size_block = size_block # size of each repetition block (size of 'for')
		self.probability = probability # probability of each block occur (probability of 'for' occur)
		self.num_words = num_words # number of words for each block
		self.n = n # number of available opcodes


	def gerarArquivo(self):
		separator = " "
		array = []
		repetition = []

		# repetition block
		for i in range(self.size_block):
			r_value = randint(1, self.n)
			s = ""
			for j in range(self.num_address[r_value - 1]):
				s = s + separator + str(randint(0, self.size_memory-1)) + separator + str(randint(0, self.num_words-1))
			repetition.append(str(r_value) + s)

		# print(repetition) # uncomment to view repetition block

		# iteration
		num_blocks = self.instructions / self.size_block;
		num_blocks_with_probability = (self.probability / 100.0 +0.0001) * (num_blocks) # "+0.0001" for round up
		i = 0
		k = 0
		while(i < self.instructions):
			if(k <= num_blocks_with_probability and i + self.size_block < self.instructions):
				i = i + self.size_block 	# increment number of instructions in each block to not exceed the total
				k = k + 1 			# increment 1 repetition block until the probability is satisfied
				array.extend(repetition)
			else:
				i = i + 1
				r_value = randint(1, self.n)
				s = ""
				for j in range(self.num_address[r_value - 1]):
					s = s + separator + str(randint(0, self.size_memory-1)) + separator + str(randint(0, self.num_words-1))
				array.append(str(r_value) + s)

		# print(array) # uncomment to view final array

		# -------------------------- File --------------------------
		file = open("instructions.txt", "w")

		# write the number of instructions at the begining of the file
		file.write(str(self.instructions))
		file.write("\n")

		# write the instructions
		for i in range(len(array)):
			file.write(array[i])
			file.write("\n")

		file.close()

experimento = Generator(8192, 10001, 10, 80, 4, 2)
experimento.gerarArquivo()
