# -*- coding: utf-8 -*-

from tp3 import *
from generator import *

def lerArquivoExperimento():
    cenarios = []
    file = open("C:\python\experimento.txt", "r")
    for linha in file:
        dado = ""
        array = []
        for char in linha:
            if char == " " or char == "\n":
                array.append(int(dado))
                dado = ""
            else:
                dado += char
        cenarios.append(array)
    file.close()

    resultadoAlgoritimo = []
    for cenario in cenarios:#LRU
        resultadoCenario = []
        gerador = Generator(cenario[9], cenario[1], cenario[2], cenario[3], cenario[4], cenario[5])
        gerador.gerarArquivo()
        maquina = Maquina(cenario[0], cenario[1], cenario[4], cenario[6], cenario[7], cenario[8], cenario[9])
        maquina.setAlgoritmoSubstituicao("LRU")
        maquina.processador("programa")
        resultadoCenario.append((maquina.hitC1 * 100.00)/(cenario[1] * 3))#C1Hit percentual
        resultadoCenario.append((maquina.hitC2 * 100.00)/(cenario[1] * 3))#C2Hit percentual
        resultadoCenario.append((maquina.hitC3 * 100.00)/(cenario[1] * 3))#C3Hit percentual
        resultadoCenario.append((maquina.hitRAM * 100.00)/(cenario[1] * 3))#RAMHit percentual
        hitCaches = maquina.hitC1 + maquina.hitC2 + maquina.hitC3 + maquina.hitRAM
        resultadoCenario.append((((cenario[1] * 3) - hitCaches) * 100.00)/(cenario[1] * 3))#HitHD percentual
        resultadoCenario.append(maquina.custo)
        resultadoAlgoritimo.append(resultadoCenario)

    print("\n\nLRU")
    for resultado in resultadoAlgoritimo:
        print("Taxa C1: %.2f \ Taxa C2: %.2f \ Taxa C3: %.2f \ Taxa RAM: %.2f \ Taxa HD: %.2f Custo: %d"
        % (resultado[0], resultado[1], resultado[2], resultado[3], resultado[4], resultado[5]))


    resultadoAlgoritimo = []
    for cenario in cenarios:#LFU
        resultadoCenario = []
        gerador = Generator(cenario[0], cenario[1], cenario[2], cenario[3], cenario[4], cenario[5])
        gerador.gerarArquivo()
        maquina = Maquina(cenario[0], cenario[1], cenario[4], cenario[6], cenario[7], cenario[8], cenario[9])
        maquina.setAlgoritmoSubstituicao("\n\nLFU")
        maquina.processador("programa")
        resultadoCenario.append((maquina.hitC1 * 100.00)/(cenario[1] * 3))#C1Hit percentual
        resultadoCenario.append((maquina.hitC2 * 100.00)/(cenario[1] * 3))#C2Hit percentual
        resultadoCenario.append((maquina.hitC3 * 100.00)/(cenario[1] * 3))#C3Hit percentual
        resultadoCenario.append((maquina.hitRAM * 100.00)/(cenario[1] * 3))#RAMHit percentual
        hitCaches = maquina.hitC1 + maquina.hitC2 + maquina.hitC3 + maquina.hitRAM
        resultadoCenario.append((((cenario[1] * 3) - hitCaches) * 100.00)/(cenario[1] * 3))#HitHD percentual
        resultadoCenario.append(maquina.custo)
        resultadoAlgoritimo.append(resultadoCenario)

    print("\n\LFU")
    for resultado in resultadoAlgoritimo:
        print("Taxa C1: %.2f \ Taxa C2: %.2f \ Taxa C3: %.2f \ Taxa RAM: %.2f \ Taxa HD: %.2f Custo: %d"
        % (resultado[0], resultado[1], resultado[2], resultado[3], resultado[4], resultado[5]))


    resultadoAlgoritimo = []
    for cenario in cenarios:#FIFO
        resultadoCenario = []
        gerador = Generator(cenario[0], cenario[1], cenario[2], cenario[3], cenario[4], cenario[5])
        gerador.gerarArquivo()
        maquina = Maquina(cenario[0], cenario[1], cenario[4], cenario[6], cenario[7], cenario[8], cenario[9])
        maquina.setAlgoritmoSubstituicao("FIFO")
        maquina.processador("programa")
        resultadoCenario.append((maquina.hitC1 * 100.00)/(cenario[1] * 3))#C1Hit percentual
        resultadoCenario.append((maquina.hitC2 * 100.00)/(cenario[1] * 3))#C2Hit percentual
        resultadoCenario.append((maquina.hitC3 * 100.00)/(cenario[1] * 3))#C3Hit percentual
        resultadoCenario.append((maquina.hitRAM * 100.00)/(cenario[1] * 3))#RAMHit percentual
        hitCaches = maquina.hitC1 + maquina.hitC2 + maquina.hitC3 + maquina.hitRAM
        resultadoCenario.append((((cenario[1] * 3) - hitCaches) * 100.00)/(cenario[1] * 3))#HitHD percentual
        resultadoCenario.append(maquina.custo)
        resultadoAlgoritimo.append(resultadoCenario)

    print("\n\n\FIFO")
    for resultado in resultadoAlgoritimo:
        print("Taxa C1: %.2f \ Taxa C2: %.2f \ Taxa C3: %.2f \ Taxa RAM: %.2f \ Taxa HD: %.2f Custo: %d"
        % (resultado[0], resultado[1], resultado[2], resultado[3], resultado[4], resultado[5]))

lerArquivoExperimento()
